﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

class State
{
    public BigBoard board;
    public int visitCount;
    public double winScore;
    public (int x, int y) move = (-1, -1);
    
    public State(BigBoard bBoard)
    {
        board = new BigBoard(bBoard);
    }

    public State(State state)
    {
        board = new BigBoard(state.board);
        visitCount = state.visitCount;
        winScore = state.winScore;
        if (state.move.x != -1 && state.move.y != -1)
            move = (state.move.x, state.move.y);
    }

    public static List<State> GetAllPossibleStates(State state)
    {
        List<State> states = new List<State>();


        List<(int x, int y)> moves = state.board.GetAvailableMoves();

        for (int i = 0; i < moves.Count; i++)
        {
            State newState = new State(state); // Make copy of current state
            newState.board.SetPlaceOwned(moves[i].x, moves[i].y);
            newState.move = (moves[i].x, moves[i].y);
            states.Add(newState);
        }

        return states;
    }

    public int simulateRandomPlayout()
    {
        // Get a list of all posiible positions on the board and play a random move until terminate state
        BigBoard tempBoard = new BigBoard(board);
        List<(int x, int y)> moves = tempBoard.GetAvailableMoves();

        Random rnd = new Random();

        while (tempBoard.status == 0 && moves.Count > 0)
        {
            int rndIndex = rnd.Next(moves.Count);
            tempBoard.SetPlaceOwned(moves[rndIndex].x, moves[rndIndex].y);
            moves = tempBoard.GetAvailableMoves();
        }

        if (tempBoard.status == 0)
        {
            if (tempBoard.playerBoardWins > tempBoard.opponendBoardWins)
            {
                tempBoard.status = 1;
            }
            else
            {
                tempBoard.status = 2;
            }
        }

        return tempBoard.status;
    }
}

class Node
{
    public State state;
    public Node parent;
    public List<Node> childArray = new List<Node>();

    public Node(State nState)
    {
        state = new State(nState);
    }

    public Node(Node node)
    {
        state = new State(node.state);
        parent = new Node(node.parent);
        childArray = node.childArray.ToList();
    }

    public Node GetRandomChildNode()
    {
        return childArray[new Random().Next(childArray.Count)];
    }

    public Node GetChildWithHighestScore()
    {
        int maxIndex = 0;
        double maxScore = childArray[0].state.winScore;

        for (int i = 0; i < childArray.Count; i++)
        {
            double childScore = childArray[i].state.winScore;
            if (childScore > maxScore)
            {
                maxScore = childScore;
                maxIndex = i;
            }
        }

        return childArray[maxIndex];
    }

    public Node GetChildByMove((int x, int y) move)
    {
        for (int i = 0; i < childArray.Count; i++)
        {
            if (move.x == childArray[i].state.move.x && move.y == childArray[i].state.move.y)
            {
                return childArray[i];
            }
        }

        return null;
    }

    public void expand()
    {
        List<State> possibleStates = State.GetAllPossibleStates(state);
        for (int i = 0; i < possibleStates.Count; i++)
        {
            Node newNode = new Node(possibleStates[i]);
            newNode.parent = this;

            childArray.Add(newNode);
        }
    }
}

class UCT
{
    public static double uctValue(int totalVisit, double nodeWinScore, int nodeVisit)
    {
        if (nodeVisit == 0)
            return Int32.MaxValue;

        return (nodeWinScore / (double)nodeVisit) + 1.41 * Math.Sqrt(Math.Log(totalVisit) / (double)nodeVisit);
    }

    public static Node findBestNodeWithUCT(Node node)
    {
        int parentVisit = node.state.visitCount;
        return node.childArray.OrderByDescending(x => uctValue(parentVisit, x.state.winScore, x.state.visitCount)).First();
    }
}

class MonteCarloTreeSearch
{
    static int WIN_SCORE = 2;
    static int DRAW_SCORE = 1;

    public Node lastNode;

    public (int x, int y) findNextMove(BigBoard board, int time)
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        Node root = new Node(new State(new BigBoard(board)));

        // Check if we can use a previous node
        if (lastNode != null)
        {
            (int x, int y) lastMove = (board.lastTurn.x, board.lastTurn.y);
            Node child = lastNode.GetChildByMove(lastMove);

            if (child != null) // See of we even discovered that node
            {
                child.parent = null; // We do not need previous information
                root = child; // Replace the root with the old node
            }
        }

        TimeSpan timeS = TimeSpan.FromMilliseconds(time);
        while (stopwatch.Elapsed < timeS)
        {
            // Phase 1 - selection
            Node promisingNode = SelectPromisingNode(root);

            // Phase 2 - Expansion
            if (promisingNode.state.board.status == 0 && promisingNode.state.board.GetAvailableMoves().Count > 0)
            {
                promisingNode.expand();
            }

            Node nodeToExplore = promisingNode;
            if (nodeToExplore.childArray.Count > 0)
                nodeToExplore = nodeToExplore.GetRandomChildNode();

            // Phase 3 - Simulation
            int playoutResult = nodeToExplore.state.simulateRandomPlayout();

            // Phase 4 - Update

            BackPropagation(nodeToExplore, playoutResult);
        }

        // Return what we found as the best move in the time we had for this
        Console.Error.WriteLine($"Itterations: {root.state.visitCount}");
        if (root.childArray.Count > 0)
        {
            Node winner = root.GetChildWithHighestScore();
            lastNode = winner;
            Console.Error.WriteLine($"Win Score: {winner.state.winScore}");
            Console.Error.WriteLine($"Move Visit Count: {winner.state.visitCount}");

            return (winner.state.move.x, winner.state.move.y);
        }
        else
        {
            lastNode = null;
            List<(int x, int y)> moves = root.state.board.GetAvailableMoves();
            int rndIndex = new Random().Next(moves.Count);
            return (moves[rndIndex].x, moves[rndIndex].y);
        }
    }

    public Node SelectPromisingNode(Node rootNode)
    {
        Node node = rootNode;

        while (node.childArray.Count != 0)
        {
            node = UCT.findBestNodeWithUCT(node);
        }

        return node;
    }

    public void BackPropagation(Node nodeToExplore, int playoutResult)
    {
        Node tempNode = nodeToExplore;

        while (tempNode.parent != null)
        {
            tempNode.state.visitCount++;

            if (playoutResult == 1)
            {
                tempNode.state.winScore += WIN_SCORE;
            }
            else if (playoutResult == 0)
            {
                tempNode.state.winScore += DRAW_SCORE;
            }

            tempNode = tempNode.parent;
        }

        // Also update the root node
        tempNode.state.visitCount++;

        if (playoutResult == 1)
        {
            tempNode.state.winScore += WIN_SCORE;
        }
        else if (playoutResult == 0)
        {
            tempNode.state.winScore += DRAW_SCORE;
        }
    }
}

// Game Stuff
class SmallBoard
{
    public int[,] board = new int[3, 3];
    public int status = 0; // Who has won the board
    public int turns = 0;

    public bool[,] diagnals = new bool[3, 3] {
        {true, false, true},
        {false, true, false},
        {true, false, true}
    };

    public SmallBoard()
    {
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                board[row, col] = 0;
            }
        }
    }

    public SmallBoard(SmallBoard smallBoard)
    {
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                board[row, col] = smallBoard.board[row, col];
            }
        }
        status = smallBoard.status;
        turns = smallBoard.turns;
        diagnals = smallBoard.diagnals;
    }

    public List<(int x, int y)> GetAvailableMoves()
    {
        List<(int x, int y)> moves = new List<(int x, int y)>();
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                if (board[row, col] == 0)
                {
                    moves.Add((row, col));
                }
            }
        }
        return moves;
    }

    public int GetWinner()
    {
        for (int i = 0; i < 3; i++)
        {
            // Check rows
            if (board[i, 0] > 0 && board[i, 0] == board[i, 1] && board[i, 0] == board[i, 2])
            {
                return board[i, 0];
            }

            // Check columns
            if (board[0, i] > 0 && board[0, i] == board[1, i] && board[0, i] == board[2, i])
            {
                return board[0, i];
            }
        }

        // Check diagnals
        if (board[0, 0] > 0 && board[0, 0] == board[1, 1] && board[0, 0] == board[2, 2])
        {
            return board[0, 0];
        }

        if (board[2, 0] > 0 && board[2, 0] == board[1, 1] && board[2, 0] == board[0, 2])
        {
            return board[2, 0];
        }

        return 0;
    }

    public void CheckWinningMove(int x, int y)
    {
        if (board[x, y] != 0)
        {
            int playerWin = 0;
            int opponendWin = 0;

            for (int i = 0; i < 3; i++)
            {
                if (board[i, y] == 1)
                    playerWin++;
                else if (board[i, y] == 2)
                    opponendWin++;
            }

            if (playerWin == 3)
            {
                status = 1;
                // Console.Error.WriteLine($"Player wins board!");
                return;
            }
            else if (opponendWin == 3)
            {
                status = 2;
                // Console.Error.WriteLine($"Opponend wins board!");
                return;
            }


            playerWin = 0;
            opponendWin = 0;

            for (int i = 0; i < 3; i++)
            {
                if (board[x, i] == 1)
                    playerWin++;
                else if (board[x, i] == 2)
                    opponendWin++;
            }

            if (playerWin == 3)
            {
                status = 1;
                // Console.Error.WriteLine($"Player wins board!");
                return;
            }
            else if (opponendWin == 3)
            {
                status = 2;
                // Console.Error.WriteLine($"Opponend wins board!");
                return;
            }

            if (diagnals[x, y])
            {
                playerWin = 0;
                opponendWin = 0;

                if (x == 1 && y == 1)
                {
                    if (board[0, 0] == 1)
                        playerWin++;
                    else if (board[0, 0] == 2)
                        opponendWin++;

                    if (board[1, 1] == 1)
                        playerWin++;
                    else if (board[1, 1] == 2)
                        opponendWin++;

                    if (board[2, 2] == 1)
                        playerWin++;
                    else if (board[2, 2] == 2)
                        opponendWin++;

                    if (playerWin == 3)
                    {
                        status = 1;
                        // Console.Error.WriteLine($"Player wins board!");
                        return;
                    }
                    if (opponendWin == 3)
                    {
                        status = 2;
                        // Console.Error.WriteLine($"Opponend wins board!");
                        return;
                    }

                    playerWin = 0;
                    opponendWin = 0;

                    if (board[0, 2] == 1)
                        playerWin++;
                    else if (board[0, 2] == 2)
                        opponendWin++;

                    if (board[1, 1] == 1)
                        playerWin++;
                    else if (board[1, 1] == 2)
                        opponendWin++;

                    if (board[2, 0] == 1)
                        playerWin++;
                    else if (board[2, 0] == 2)
                        opponendWin++;

                    if (playerWin == 3)
                    {
                        status = 1;
                        // Console.Error.WriteLine($"Player wins board!");
                        return;
                    }
                    else if (opponendWin == 3)
                    {
                        status = 2;
                        // Console.Error.WriteLine($"Opponend wins board!");
                        return;
                    }
                }
                else
                {
                    if (board[1, 1] == 1)
                        playerWin++;
                    else if (board[1, 1] == 2)
                        opponendWin++;

                    if (board[x, y] == 1)
                        playerWin++;
                    else if (board[x, y] == 2)
                        opponendWin++;

                    int tmpRow = 0;
                    int tmpCol = 0;

                    if (x == 0)
                        tmpRow = 2;

                    if (y == 0)
                        tmpCol = 2;

                    if (board[tmpRow, tmpCol] == 1)
                        playerWin++;
                    else if (board[tmpRow, tmpCol] == 2)
                        opponendWin++;

                    if (playerWin == 3)
                    {
                        status = 1;
                        // Console.Error.WriteLine($"Player wins board!");
                        return;
                    }
                    else if (opponendWin == 3)
                    {
                        status = 2;
                        // Console.Error.WriteLine($"Opponend wins board!");
                        return;
                    }
                }
            }
        }
    }

    public void SetPlaceOwned(int row, int col, bool player)
    {
        board[row, col] = player ? 1 : 2;
        turns++;
        //CheckWinningMove(row, col);
        status = GetWinner();
    }
}

class BigBoard
{
    public SmallBoard[,] board = new SmallBoard[3, 3];
    public int status = 0;
    public bool playerTurn;
    public int playerBoardWins = 0;
    public int opponendBoardWins = 0;

    public (int x, int y) lastTurn = (-1, -1);

    public bool[,] diagnals = new bool[3, 3] {
        {true, false, true},
        {false, true, false},
        {true, false, true}
    };

    public BigBoard()
    {
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                board[row, col] = new SmallBoard();
            }
        }
    }

    public BigBoard(BigBoard bBoard)
    {
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                board[row, col] = new SmallBoard(bBoard.board[row, col]);
            }
        }
        status = bBoard.status;
        playerTurn = bBoard.playerTurn;
        opponendBoardWins = bBoard.opponendBoardWins;
        lastTurn = (bBoard.lastTurn.x, bBoard.lastTurn.y);
        diagnals = bBoard.diagnals;
    }

    public List<(int x, int y)> GetAvailableMoves()
    {
        List<(int x, int y)> moves = new List<(int x, int y)>();

        if (lastTurn.x != -1 && lastTurn.y != -1)
        {
            // Small board coord
            int smallRow = lastTurn.x % 3;
            int smallCol = lastTurn.y % 3;

            if (board[smallRow, smallCol].status == 0 && board[smallRow, smallCol].turns < 9)
            {
                // We are forced to place our next move in this small board acording to the last turn
                List<(int x, int y)> smallMoves = board[smallRow, smallCol].GetAvailableMoves();
                for (int i = 0; i < smallMoves.Count; i++)
                {
                    moves.Add((smallMoves[i].x + 3 * smallRow, smallMoves[i].y + 3 * smallCol));
                }
                return moves;
            }
        }

        // We can decide which small board we want to place in.
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                SmallBoard sBoard = board[row, col];
                if (sBoard.status == 0 && sBoard.turns < 9)
                {
                    List<(int x, int y)> smallMoves = sBoard.GetAvailableMoves();
                    for (int i = 0; i < smallMoves.Count; i++)
                    {
                        moves.Add((smallMoves[i].x + 3 * row, smallMoves[i].y + 3 * col));
                    }
                }
            }
        }

        return moves;
    }

    public int GetWinner()
    {
        for (int i = 0; i < 3; i++)
        {
            // Check rows
            if (board[i, 0].status > 0 && board[i, 0].status == board[i, 1].status && board[i, 0].status == board[i, 2].status)
            {
                return board[i, 0].status;
            }

            // Check columns
            if (board[0, i].status > 0 && board[0, i].status == board[1, i].status && board[0, i].status == board[2, i].status)
            {
                return board[0, i].status;
            }
        }

        // Check diagnals
        if (board[0, 0].status > 0 && board[0, 0].status == board[1, 1].status && board[0, 0].status == board[2, 2].status)
        {
            return board[0, 0].status;
        }

        if (board[2, 0].status > 0 && board[2, 0].status == board[1, 1].status && board[2, 0].status == board[0, 2].status)
        {
            return board[2, 0].status;
        }

        return 0;
    }

    public void CheckWinningMove(int x, int y)
    {
        if (board[x, y].status != 0)
        {
            int playerWin = 0;
            int opponendWin = 0;

            for (int i = 0; i < 3; i++)
            {
                if (board[i, y].status == 1)
                    playerWin++;
                else if (board[i, y].status == 2)
                    opponendWin++;
            }

            if (playerWin == 3)
            {
                status = 1;
                // Console.Error.WriteLine($"Player wins board!");
                return;
            }
            else if (opponendWin == 3)
            {
                status = 2;
                // Console.Error.WriteLine($"Opponend wins board!");
                return;
            }

            playerWin = 0;
            opponendWin = 0;

            for (int i = 0; i < 3; i++)
            {
                if (board[x, i].status == 1)
                    playerWin++;
                else if (board[x, i].status == 2)
                    opponendWin++;
            }

            if (playerWin == 3)
            {
                status = 1;
                // Console.Error.WriteLine($"Player wins board!");
                return;
            }
            else if (opponendWin == 3)
            {
                status = 2;
                // Console.Error.WriteLine($"Opponend wins board!");
                return;
            }

            if (diagnals[x, y])
            {
                playerWin = 0;
                opponendWin = 0;

                if (x == 1 && y == 1)
                {
                    if (board[0, 0].status == 1)
                        playerWin++;
                    else if (board[0, 0].status == 2)
                        opponendWin++;

                    if (board[1, 1].status == 1)
                        playerWin++;
                    else if (board[1, 1].status == 2)
                        opponendWin++;

                    if (board[2, 2].status == 1)
                        playerWin++;
                    else if (board[2, 2].status == 2)
                        opponendWin++;

                    if (playerWin == 3)
                    {
                        status = 1;
                        // Console.Error.WriteLine($"Player wins board!");
                        return;
                    }
                    if (opponendWin == 3)
                    {
                        status = 2;
                        // Console.Error.WriteLine($"Opponend wins board!");
                        return;
                    }

                    playerWin = 0;
                    opponendWin = 0;

                    if (board[0, 2].status == 1)
                        playerWin++;
                    else if (board[0, 2].status == 2)
                        opponendWin++;

                    if (board[1, 1].status == 1)
                        playerWin++;
                    else if (board[1, 1].status == 2)
                        opponendWin++;

                    if (board[2, 0].status == 1)
                        playerWin++;
                    else if (board[2, 0].status == 2)
                        opponendWin++;

                    if (playerWin == 3)
                    {
                        status = 1;
                        // Console.Error.WriteLine($"Player wins board!");
                        return;
                    }
                    else if (opponendWin == 3)
                    {
                        status = 2;
                        // Console.Error.WriteLine($"Opponend wins board!");
                        return;
                    }
                }
                else
                {
                    if (board[1, 1].status == 1)
                        playerWin++;
                    else if (board[1, 1].status == 2)
                        opponendWin++;

                    if (board[x, y].status == 1)
                        playerWin++;
                    else if (board[x, y].status == 2)
                        opponendWin++;

                    int tmpRow = 0;
                    int tmpCol = 0;

                    if (x == 0)
                        tmpRow = 2;

                    if (y == 0)
                        tmpCol = 2;

                    if (board[tmpRow, tmpCol].status == 1)
                        playerWin++;
                    else if (board[tmpRow, tmpCol].status == 2)
                        opponendWin++;

                    if (playerWin == 3)
                    {
                        status = 1;
                        // Console.Error.WriteLine($"Player wins board!");
                        return;
                    }
                    else if (opponendWin == 3)
                    {
                        status = 2;
                        // Console.Error.WriteLine($"Opponend wins board!");
                        return;
                    }
                }
            }
        }
    }

    public void SetPlaceOwned(int row, int col)
    {

        int smallRow = row % 3;
        int smallCol = col % 3;
        int bigRow = (row - smallRow) / 3;
        int bigCol = (col - smallCol) / 3;

        board[bigRow, bigCol].SetPlaceOwned(smallRow, smallCol, playerTurn);
        //CheckWinningMove(bigRow, bigCol);
        status = GetWinner();
        playerTurn = !playerTurn;
        lastTurn.x = row;
        lastTurn.y = col;

        if (board[bigRow, bigCol].status == 1)
            playerBoardWins++;
        if (board[bigRow, bigCol].status == 2)
            opponendBoardWins++;
    }
}

class Player
{
    static void Main(string[] args)
    {
        PlayYourself(70);
        //PlayBot(80);
        //PlayCodinGame();

        // TESTS
        // -------------------------------------------------------------
        //SmallBoardWinTest();
    }

    public static void SmallBoardWinTest()
    {
        SmallBoard board = new SmallBoard();
        board.board = new int[,]
        {
            {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1}
        };
        Console.Error.WriteLine($"{board.GetWinner()}");
        board.board = new int[,]
        {
            {0, 0, 1},
            {0, 1, 0},
            {1, 0, 0}
        };
        Console.Error.WriteLine($"{board.GetWinner()}");
        board.board = new int[,]
        {
            {1, 1, 1},
            {0, 0, 0},
            {0, 0, 0}
        };
        Console.Error.WriteLine($"{board.GetWinner()}");
        board.board = new int[,]
        {
            {0, 0, 0},
            {1, 1, 1},
            {0, 0, 0}
        };
        Console.Error.WriteLine($"{board.GetWinner()}");
        board.board = new int[,]
        {
            {0, 0, 0},
            {0, 0, 0},
            {1, 1, 1}
        };
        Console.Error.WriteLine($"{board.GetWinner()}");
        board.board = new int[,]
        {
            {1, 0, 0},
            {1, 0, 0},
            {1, 0, 0}
        };
        Console.Error.WriteLine($"{board.GetWinner()}");
        board.board = new int[,]
        {
            {0, 1, 0},
            {0, 1, 0},
            {0, 1, 0}
        };
        Console.Error.WriteLine($"{board.GetWinner()}");
        board.board = new int[,]
        {
            {0, 0, 1},
            {0, 0, 1},
            {0, 0, 1}
        };
        Console.Error.WriteLine($"{board.GetWinner()}");
        Console.ReadKey();
    }

    public static void PlayCodinGame()
    {
        string[] inputs;

        BigBoard board = new BigBoard();
        board.playerTurn = true;
        MonteCarloTreeSearch MCTS = new MonteCarloTreeSearch();

        // game loop
        while (true)
        {
            inputs = Console.ReadLine().Split(' ');
            int opponentRow = int.Parse(inputs[0]);
            int opponentCol = int.Parse(inputs[1]);
            if (opponentRow != -1 && opponentCol != -1)
            {
                board.playerTurn = false;
                board.SetPlaceOwned(opponentRow, opponentCol);
            }

            int validActionCount = int.Parse(Console.ReadLine());
            int[,] validPlaces = new int[validActionCount, 2];
            for (int i = 0; i < validActionCount; i++)
            {
                inputs = Console.ReadLine().Split(' ');
                validPlaces[i, 0] = int.Parse(inputs[0]);
                validPlaces[i, 1] = int.Parse(inputs[1]);
            }

            // Write an action using Console.WriteLine()
            // To debug: Console.Error.WriteLine("Debug messages...");

            (int x, int y) tmp = MCTS.findNextMove(board, 70);
            board.SetPlaceOwned(tmp.x, tmp.y);
            Console.WriteLine($"{tmp.x} {tmp.y}");
            //Console.WriteLine("0 0");
        }
    }

    public static void PlayYourself(int botTime)
    {
        BigBoard board = new BigBoard();
        board.playerTurn = true;

        MonteCarloTreeSearch MCTS = new MonteCarloTreeSearch();

        while (board.status == 0)
        {
            (int x, int y) tmp = MCTS.findNextMove(board, botTime);
            board.SetPlaceOwned(tmp.x, tmp.y);

            PrintBoard(board);

            string[] inputs = Console.ReadLine().Split(' ');
            int opponentRow = int.Parse(inputs[0]);
            int opponentCol = int.Parse(inputs[1]);
            board.SetPlaceOwned(opponentRow, opponentCol);
        }

        Console.Error.WriteLine($"{board.status} Won");
        Console.ReadKey();
    }

    public static void PlayBot(int botTime)
    {
        BigBoard board = new BigBoard();
        board.playerTurn = true;

        MonteCarloTreeSearch MCTS1 = new MonteCarloTreeSearch();
        MonteCarloTreeSearch MCTS2 = new MonteCarloTreeSearch();

        while (board.status == 0)
        {
            (int x, int y) tmp = MCTS1.findNextMove(board, botTime);
            board.SetPlaceOwned(tmp.x, tmp.y);

            PrintBoard(board);
            tmp = MCTS2.findNextMove(board, botTime);
            board.SetPlaceOwned(tmp.x, tmp.y);
        }

        Console.Error.WriteLine($"{board.status} Won");
        Console.ReadKey();
    }

    public static void PrintBoard(BigBoard board)
    {
        string[] buf = new string[9] { "0 ", "1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 "};

        List<(int x, int y)> moves = board.GetAvailableMoves();

        for (int row = 0; row < 3; row++)
        {
            for (int sRow = 0; sRow < 3; sRow++)
            {
                for (int col = 0; col < 3; col++)
                {

                    for (int sCol = 0; sCol < 3; sCol++)
                    {
                        string x = $"{board.board[row, col].board[sRow, sCol]}";
                        if (x == "0")
                        {
                            int[] move = new int[] { sRow + 3 * row, sCol + 3 * col };
                            if (moves.Any(m => $"{m.x} {m.y}" == $"{sRow + 3 * row} {sCol + 3 * col}"))
                            {
                                x = ".";
                            }
                            else
                            {
                                x = " ";
                            }
                        }
                        else if (x == "1")
                        {
                            x = "X";
                        }
                        else if (x == "2")
                        {
                            x = "O";
                        }
                        buf[row * 3 + sRow] += x;
                    }
                    if (col < 2)
                        buf[row * 3 + sRow] += "|";
                }
            }
        }

        Console.Error.WriteLine("  012 345 678");
        for (int i = 0; i < buf.Length; i++)
        {
            if (i == 3 || i == 6)
            {
                Console.Error.WriteLine("  ---+---+---");
            }

            Console.Error.WriteLine(buf[i]);
        }
    }
}